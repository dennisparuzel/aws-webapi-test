﻿using Ryanair.Reservation.Domain;
using Ryanair.Reservation.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using Ryanair.Reservation.ApplicationService.Interfaces;

namespace Ryanair.Reservation.ApplicationService
{
    public class FlightQueryService : IFlightQueryService
    {
        private readonly IFlightRepository _flightRepository;

        public FlightQueryService(IFlightRepository flightRepository)
        {
            this._flightRepository = flightRepository;
        }

        public List<Flight> Get(int passengers, string origin, string destination, DateTime dateOut, DateTime dateIn, bool roundTrip)
        {
            return this._flightRepository.Get(passengers, origin, destination, dateOut, dateIn, roundTrip).ToList();
        }
    }
}

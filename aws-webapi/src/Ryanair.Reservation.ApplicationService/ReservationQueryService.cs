﻿using Ryanair.Reservation.ApplicationService.Interfaces;
using Ryanair.Reservation.Repositories;

namespace Ryanair.Reservation.ApplicationService
{
    public class ReservationQueryService : IReservationQueryService
    {
        private readonly IReservationRepository _reservationRepository;

        public ReservationQueryService(IReservationRepository reservationRepository)
        {
            this._reservationRepository = reservationRepository;
        }

        public Domain.Reservation GetByReservationKey(string reservationKey)
        {
            return this._reservationRepository.GetByReservationKey(reservationKey);
        }
    }
}

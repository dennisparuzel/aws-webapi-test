﻿namespace Ryanair.Reservation.Domain
{
    public class Passenger
    {
        public string Name { get; }

        public int Bags { get; }

        public string Seat { get; }
    }
}

﻿namespace Ryanair.Reservation.Repositories
{
    public interface IReservationRepository 
    {
        Domain.Reservation GetByReservationKey(string reservationKey);
    }
}

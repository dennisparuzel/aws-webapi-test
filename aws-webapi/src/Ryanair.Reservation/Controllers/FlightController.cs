﻿using Microsoft.AspNetCore.Mvc;
using Ryanair.Reservation.ApplicationService;
using Ryanair.Reservation.Models;
using System.Collections.Generic;
using Ryanair.Reservation.ApplicationService.Interfaces;

namespace Ryanair.Reservation.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class FlightController : ControllerBase
    {
        private readonly IFlightQueryService _flightQueryService;

        public FlightController(IFlightQueryService flightQueryService)
        {
            _flightQueryService = flightQueryService;
        }

        [HttpGet]
        public ActionResult<List<Models.Flight>> Get([FromQuery] GetFlights getFlights)
        {
            var flights = _flightQueryService.Get(getFlights.Passengers, getFlights.Origin, getFlights.Destination, getFlights.DateOut, getFlights.DateIn, getFlights.RoundTrip);

            //TODO: Add some sort of mapping from Domain to Model
            return new List<Models.Flight>();
        }
    }
}

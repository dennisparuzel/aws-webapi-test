# **AWS Practice Test Work log**

## Overview

 Feel free to add everything you consider in order to understand why you implement those approaches and what are the benefits them in terms of:

1. Operational
1. Security
1. Reliability
1. Performance
1. Cost optimization

### Part 01 Explain all steps of Deploy serverless architecture

#### 01 - Steps to achive goals

*write here*

#### 01 - Beneficts of you solution

*write here*

#### 01 - Notes

*write here*

### Part 03 Explain all steps of Deploy Ryanair API on EC2 instances

#### 03 - Steps to achive goals

*write here*

#### 03 - Beneficts of you solution

*write here*

#### 03 - Link your architecture diagram of your solution

*write here*

#### 03 -Notes

*write here*


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace dotnetlambda
{
    public class Retrieve
    {
        
        /// <summary>
        /// A simple retrieve function 
        /// </summary>
        /// <returns></returns>
        /// Paste your code here.

    }
}